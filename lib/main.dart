import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  //const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      backgroundColor: Colors.green[400],
      appBar: AppBar(
        title: Text("I am Rich"),
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Image(
          image: AssetImage("assets/images/index1.jpg"),
        ),
      ),
    ));
  }
}
